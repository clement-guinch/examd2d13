
var setDelete = function(element){
    element.addEventListener('click', function() {
      makeRequest(this.getAttribute('data-id'), 'DELETE', null, function(res) {
         alert('effacé');
         element.closest('aside').remove();
      });
    })
};
document.querySelectorAll('.delete').forEach(setDelete);

document.getElementById('editGare').addEventListener('submit', function(evt) {
  evt.preventDefault();
  var form = evt.target;
  var datas = {};

  form.querySelectorAll('[nom]').forEach(function(el) {
    datas[el.getAttribute('nom')] = el.value
  });
  makeRequest('', form.getAttribute('method'), datas, function(res){
    alert('Gare ajoutée');
    var span = document.createElement("span");
    span.innerHTML = res.response;
    var buttonSubmit = span.querySelector(".submit");
    var container = document.getElementById('container');
    container.appendChild(span.firstChild);
  })
});

// form.querySelectorAll('[name]').forEach(function(el) {
//   datas[el.getAttribute('name')] = el.value
// });
// makeRequest('', form.getAttribute('method'), datas, function(res){
//   alert('Modification ok');
//   var span = document.createElement("span");
//   span.innerHTML = res.response;
//   var buttonSubmit = span.querySelector(".submit");
//   var container = document.getElementById('container');
//   container.appendChild(span.firstChild);
// })
// });


function makeRequest(id, method, datas, callback) {
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
            callback(JSON.parse(httpRequest.responseText));
          } else {
            alert('There was a problem with the request.');
          }
        }
    };
    httpRequest.open(method, '/gares/' + id);
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.send(datas ? JSON.stringify(datas) : null);
  }
