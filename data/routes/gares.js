var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

MongoClient.connect('mongodb://localhost:27017/examD2D13', { useNewUrlParser: true }, function (err, client) {
  let db = client.db('examD2D13')
  router.get('/', function(req, res, next) {
    db.collection('gares').find().toArray(function (err, result) {
    res.render('gares', { gares : result });
      });
  });

  router.use(function(req, res, next){
    res.setHeader('Content-Type', 'application/json');
    next()
  });

  router.delete('/:id', function(req, res, next) {
    db.collection('gares').deleteOne({_id : ObjectId(req.params.id)},function (err, result) {
      if(err) return next(err);
      return res.json(result);
    })
  });

  router.post('/', function(req, res, next) {
    db.collection('gares').insertOne({ nom : req.body.nom, datecreation : new Date(), siret : req.body.siret, theme : 'DEPLACEMENT', idexterne : req.body.idexterne, soustheme : 'Gare Ferroviaire', gid : req.body.gid, identifiant : req.body.identifiant }, function(err, result) {
      db.collection('gares').findOne({_id : result.insertedId }, function(err, doc) {
        res.render('gares', { gare : doc }, function(err, html) {
          return res.json({
            response : html
            })
          })
        })
      })
    });
  });

module.exports = router;
